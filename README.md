A Personal custom class addon for Deathstrider, made accesible that is about an alien female character, centered for a Flesh & Steel playthrough (No magic), as well as possibly a middle ground choice between the base classes, or ultimately to be used as another follower.

Playing this class differs from the base F&S class experience, being slightly more casual at the expense of other traits that Wraith has and Nathaly does not, listing the details you need to know when playing her:

-Body composed of three parts (like Solomon), not limited to durability however, with slightly different bonuses as well. Blood bar acts the same as Wraith's

-No emergency heel beam, this class does not have it.

-More stamina (weight capacity) than Wraith, but less inventory slots (three rows), learn to use the backpack if you are a hoarder.

-Slightly faster movement speed than base classes when in optimal condition. Slightly shorter height as well, meaning you may be able to have some favorable covers depending the spot but also be unable to reach some higher ledges.

-Different starting gear, no vipers at all, instead you get a Raptor rifle to start with, a breaching charge to compensate for the lack of a heel beam, and a flashlight.

Credits:
All player sprites, except the gore sequence, were drawn from scratch by me.

Some sounds taken from DemonSteele mod.
Any other sound I do not remember where are they from, sorry, the class was done long ago for other mod at first and I didn't keep track of the source of the sounds.

Credit to Zhs2 for being patient and helping me to set up this class and for Ace as well for responding some of my questions related to make this class possible.

-To do:
-Probably darken the sprites in general.

DISCLAIMER:
-If something breaks or is super stupidly written, might take a bit more than usual to fix something, I implemented the code by copying the gods and anything that is there for no reason, I'm just too regarded to understand, or maybe I have not updated the master in a while to even realize, please leave an issue if so and thanks.